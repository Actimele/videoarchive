<?php

namespace app\controllers;

use app\models\VideoArchiveSearch;
use yii\filters\AccessControl;
use Yii;
use app\models\VideoArchive;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \mPDF;
/**
 * VideoArchiveController implements the CRUD actions for VideoArchive model.
 */
class VideoArchiveController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VideoArchive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideoArchiveSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single VideoArchive model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VideoArchive model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VideoArchive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VideoArchive model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VideoArchive model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDestroy($id) {
        $this->findModel($id)->destroyDrive();
        return $this->redirect(['index']);
    }
    
    public function actionReport() {
        //print_r("sdaads");
        $searchModel = new VideoArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $this->layout = '//print';
        
       // $mpdf=new mPDF('utf-8', 'A4-L');
        //$mpdf->WriteHTML('<p>Hallo World</p>');
       // echo $mpdf->Output();
       // exit;
        Yii::$app->response->format = 'pdf';
        Yii::$container->set(Yii::$app->response->formatters['pdf']['class'], ['format' => 'A4-L']);        
        return $this->render('report', ['data'=>$dataProvider->getModels()]);
     
     
    }
    /**
     * Finds the VideoArchive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VideoArchive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VideoArchive::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
