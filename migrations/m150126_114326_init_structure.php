<?php

use yii\db\Schema;
use yii\db\Migration;

class m150126_114326_init_structure extends Migration
{
    public function up()
    {
        $this->createTable('video_archive', [
            'id' => 'pk',
            'drive_id'      => Schema::TYPE_INTEGER. ' NOT NULL',
            'create_date'   => Schema::TYPE_DATE. ' NOT NULL',
            'worked_organization'  => Schema::TYPE_INTEGER. ' NOT NULL' ,
            'worked_place'  => Schema::TYPE_STRING. ' NOT NULL',
            'time_lapse'    => Schema::TYPE_TIME. ' NOT NULL',
            'drive_destroy_date' => Schema::TYPE_DATE,
            'drive_destroy_employer' => Schema::TYPE_STRING
        ]);
        $this->createTable('organization', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT,
        ]);
        $this->addForeignKey('video_archive_organization', 'video_archive', 'worked_organization', 'organization', 'id');

    }

    public function down()
    {
        echo "m150126_114326_init_structure cannot be reverted.\n";

        return false;
    }
}
