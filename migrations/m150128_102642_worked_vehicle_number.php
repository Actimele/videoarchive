<?php

use yii\db\Schema;
use yii\db\Migration;

class m150128_102642_worked_vehicle_number extends Migration
{
    public function up()
    {
        $this->addColumn('video_archive', 'worked_vehicle_number', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m150128_102642_worked_vehicle_number cannot be reverted.\n";

        return false;
    }
}
