/**
 *  
 * @param {string} selector
 * @param {int} time
 * @returns {undefined}
 */
function updateTimeLapse(selector, time) {
    /** @type Date */
    var dt,
        h,
        m,
        s,
        tl = $(selector);
    dt = new Date(time*1000);
    h = dt.getUTCHours();
    m = dt.getUTCMinutes();
    s = dt.getUTCSeconds();
    
    tl.val(h+':'+m+':'+s);
};
function getLapseSeconds(){
    var time =  $('input[name="VideoArchive[time_lapse]"]').val();
    if (time.length > 0 ) {
        var tt = time.split(":");
        return tt[0] * 3600 + tt[1] * 60 + tt[2] * 1;

    }
    return 0;
}

$('#time_lapse_picker').slider({
    tooltip:'hide',
    max: 10000
}).on('change', function(e){
    updateTimeLapse('input[name="VideoArchive[time_lapse]"]', e.value.newValue);
})
$('#time_lapse_picker').slider('setValue', getLapseSeconds());
$('input[name="VideoArchive[createDateLocale]"]').datetimepicker({minView:2, format: 'dd.mm.yyyy', autoclose:true, todayBtn: true, todayHighlight: true});
$('input[name="VideoArchive[destroyDateLocale]"]').datetimepicker({minView:2, format: 'dd.mm.yyyy', autoclose:true, todayBtn: true,  todayHighlight: true});
