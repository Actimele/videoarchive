$('#GenReport').on('click', function (e) {
    e.preventDefault();
    var filters = $('.filters input');
    
    var pos = document.URL.indexOf('?');
    var url = pos < 0 ? document.URL : document.URL.substring(0, pos);
    
    $('body').find('form.gridview-filter-form').remove();
    var $form = $('<form action="' + url + '" method="get" target="_blank" class="gridview-filter-form" style="display:none" data-pjax></form>').appendTo($('body'));
    $.each(filters, function (name, value) {
        $form.append($('<input type="hidden" name="t" value="" />').attr('name', $(this).attr('name')).val($(this).attr('value')));
    });
    $form.append($('<input type="hidden" name="r" value="video-archive/report" />'));
    $form.submit();
});