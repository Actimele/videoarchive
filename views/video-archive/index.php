<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Video Archives';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="video-archive-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Video Archive', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create PDF Report', ['report'], ['class' => 'btn btn-info', 'id'=> 'GenReport']) ?>
    </p>

    <?php
        $this->registerJsFile('@web/js/VideoArchiveGrid.js', ['position'=>\yii\web\View::POS_END, 'depends'=>[yii\web\JqueryAsset::className()]]);
        $this->registerJS("
            $('input[name=\"VideoArchiveSearch[drive_destroy_date]\"]').datetimepicker({minView:2, format: 'dd.mm.yyyy', todayHighlight: true});
            $('input[name=\"VideoArchiveSearch[create_date]\"]').datetimepicker({minView:2, format: 'dd.mm.yyyy', todayHighlight: true});",
            \yii\web\View::POS_END);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
          //  'id',
            'drive_id',
            ['attribute' => 'create_date', 'format' => ['date', "d.M.Y"]],
            ['attribute' => 'title', 'value' => 'workedOrganization.title'],
            'worked_place',
            'worked_vehicle_number',
            ['attribute' => 'drive_destroy_date', 'format' => ['date', "d.M.Y"]],
            'drive_destroy_employer',
            'time_lapse',
        //    ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons'=>[
                    'delete' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-remove" title="Удалить"></span>', $url,
                            ['data-method'=>"post", 'data-confirm' => 'Are you sure?']);
                    },
                    'destroy'=>function ($url, $model, $key) {
                    return $model->drive_destroy_date === null ? Html::a('<span class="glyphicon glyphicon-trash" title="Уничтожить"></span>', 
                        Yii::$app->getUrlManager()->createUrl(['video-archive/destroy','id'=>$model['id']])) : '';
                }],
                'template'=>'{view}{update}{delete}{destroy}',
                'options' => ['width' => '80px']
            ]
        ],

    ]); ?>

</div>
