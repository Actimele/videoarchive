<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideoArchive */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-archive-form">

    <?php

    $this->registerJsFile('@web/js/bootstrap-datetimepicker.min.js', ['position' => \yii\web\View::POS_END, 'depends'=>[yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('@web/js/bootstrap-slider.js', ['position' => \yii\web\View::POS_END, 'depends'=>[yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('@web/js/VideoArchiveForm.js', ['position' => \yii\web\View::POS_END, 'depends'=>[yii\web\JqueryAsset::className()]]);
    $this->registerCssFile('@web/css/bootstrap-datetimepicker.min.css');
    $this->registerCssFile('@web/css/bootstrap-slider.css');


    $form = ActiveForm::begin(); ?>
    <script>
        
    </script>
    <?= $form->field($model, 'drive_id')->textInput() ?>

    <?= $form->field($model, 'createDateLocale')->textInput([]) ?>


    <?= $form->field($model, 'worked_organization')->dropDownList(
        yii\helpers\ArrayHelper::map(\app\models\Organization::find()->all(), 'id', 'title'), ['prompt'=>'Select please']) ?>

    <?= $form->field($model, 'worked_vehicle_number')->textInput() ?>

    <?= $form->field($model, 'worked_place')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'time_lapse')->textInput() ?><div id="time_lapse_picker"></div>

    <?= $form->field($model, 'destroyDateLocale')->textInput() ?>



    <?= $form->field($model, 'drive_destroy_employer')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
