<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VideoArchive */

$this->title = 'Update Video Archive: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Video Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="video-archive-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
