<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VideoArchive */

$this->title = 'Create Video Archive';
$this->params['breadcrumbs'][] = ['label' => 'Video Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-archive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
