<?php /** @var $data app\models\VideoArchive[] */ $c = 0;?>
<style type="text/css">
    thead td {
        text-align: center;
    }
    table {
        border-collapse: collapse;
    }
    td {
        border: 1px solid black;
    }
</style>
<h2 align="center">Форма журнала учета видеоинформации</h2>
<table>
    <thead><tr>
        <td>№ п/п</td>
        <td>Порядковый номер диска</td>
        <td>Дата поступления</td>
        <td>Наименование подрядной организации/подразделения, от которого поступила видеоинформация</td>
        <td>Место проведения работ (наименование, километр МТ)</td>
        <td>Государственный регистрационный знак, марка (модель) экскаватора/бульдозера</td>
        <td>Временной интервал записанной видеоинформации</td>
        <td>Подпись отствественного за введение архива</td>
        <td>Дата уничтожения диска</td>
        <td>Инициалы, подпись лица, уничтожевшего диск</td>
    </tr><tr class="col_num" >
        <?php for($i=0; $i<10;$i++) { echo "<td>".($i+1)."</td>"; } ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach($data as $d) { $c++;
        ?>
        <tr>
            <td><?= $c ?></td>
            <td><?= $d->drive_id ?></td>
            <td><?= ( new DateTime($d->create_date))->format("d.m.Y") ?></td>
            <td><?= $d->workedOrganization->title ?></td>
            <td><?= $d->worked_place ?></td>
            <td><?= $d->worked_vehicle_number ?></td>
            <td><?= $d->time_lapse ?></td>
            <td></td>
            <td><?= isset($d->drive_destroy_date)?( new DateTime($d->drive_destroy_date))->format("d.m.Y"):"" ?></td>
            <td><?= $d->drive_destroy_employer ?></td>
        </tr>
    <?php } ?>
    <?= str_repeat("<tr>".str_repeat("<td>&nbsp;</td>",10)."</tr>", 28-count($data)); ?>


    </tbody>
</table>
