<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video_archive".
 *
 * @property integer $id
 * @property integer $drive_id
 * @property string $create_date
 * @property integer $worked_organization
 * @property string $worked_place
 * @property string $worked_vehicle_number
 * @property string $time_lapse
 * @property string $drive_destroy_date
 * @property string $drive_destroy_employer
 *
 * @property Organization $workedOrganization
 */
class VideoArchive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drive_id', 'create_date', 'worked_organization', 'worked_place', 'time_lapse','worked_vehicle_number'], 'required'],
            [['drive_id', 'worked_organization'], 'integer'],
            [[ 'time_lapse', 'createDateLocale','destroyDateLocale'], 'safe'],
            [['createDateLocale','destroyDateLocale'], 'date', 'format' => 'php:d.m.Y'],
            [['create_date', 'drive_destroy_date'], 'date', 'format' => 'php:Y-m-d'],
            [['worked_place', 'drive_destroy_employer', 'worked_vehicle_number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'drive_id' => 'Порядковый номер диска',
            'create_date' => 'Дата поступления',
            'worked_organization' => 'Подрядная организация',
            'worked_place' => 'Место работ',
            'worked_vehicle_number' => 'Гос. номер авто',
            'time_lapse' => 'Длительность',
            'drive_destroy_date' => 'Дата уничтожения диска',
            'drive_destroy_employer' => 'Сотрудник',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkedOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'worked_organization']);
    }

    public function getCreateDateLocale() { return $this->getDateLocaleAttribute('create_date'); }
    public function setCreateDateLocale($value) { $this->setDateLocaleAttribute('create_date', $value);}

    public function getDestroyDateLocale() { return $this->getDateLocaleAttribute('drive_destroy_date');}
    public function setDestroyDateLocale($value) { $this->setDateLocaleAttribute('drive_destroy_date', $value); }

    private function getDateLocaleAttribute($attributeName) {
        $return = "";
        if (!empty($this->$attributeName)) {
            try {
                $return = \Yii::$app->formatter->asDate($this->$attributeName, 'php:d.m.Y');
            } catch (\Yii\base\InvalidParamException $e) {
                //return "";// \Yii::$app->formatter->asDate($this->getOldAttribute($attributeName), 'php:d.m.Y');
            }
        }

        return $return;
    }
    private function setDateLocaleAttribute($attributeName,  $value) {
        $this->$attributeName = $this->convertDateToLocale($value);
    }
    protected function convertDateToLocale($value) {
        if ( preg_match("/(\d{2}).(\d{2}).(\d{4})/", $value )) {
            $dt = \DateTime::createFromFormat("d.m.Y", $value);
            if ($dt !== false) {
                return $dt->format("Y-m-d");
            }
        } else
        {
            return $value;
        }
    }
    
    public function destroyDrive() {
        $this->drive_destroy_date = (new \DateTime())->format('Y-m-d');
        if (!Yii::$app->user->isGuest)
            $this->drive_destroy_employer = Yii::$app->user->identity->username;
        return $this->save();
    }
}
