<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "video_archive".
 *
 * @property integer $id
 * @property integer $drive_id
 * @property string $create_date
 * @property integer $worked_organization
 * @property string $worked_place
 * @property string $time_lapse
 * @property string $drive_destroy_date
 * @property string $drive_destroy_employer
 *
 * @property Organization $workedOrganization
 */
class VideoArchiveSearch extends VideoArchive {
    public $title;
    
    public function rules()
    {
        return [
            // String
            [['worked_place', 'drive_id', 'title','worked_vehicle_number'], 'string'],
            [['create_date', 'drive_destroy_date'], 'date', 'format' => 'php:d.m.Y']
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find()->joinWith(['workedOrganization']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['title'] = [
            'asc' => [Organization::tableName() . '.title' => SORT_ASC],
            'desc' => [Organization::tableName() . '.title' => SORT_DESC]
        ];
        
        if (!($this->load($params) && $this->validate())) {
            $query->joinWith(['workedOrganization']);
            return $dataProvider;
        }
   
         $query->andFilterWhere(
            [
                'drive_destroy_date' => $this->convertDateToLocale($this->drive_destroy_date),
                'create_date' => $this->convertDateToLocale($this->create_date),
//                'id' => $this->id,
//                'status_id' => $this->status_id,
//                'role' => $this->role,
//                'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => $this->created_at,
//                'FROM_UNIXTIME(updated_at, "%d.%m.%Y")' => $this->updated_at
            ]
        );
       
        $query->andFilterWhere(['like', VideoArchiveSearch::tableName() . '.worked_place', $this->worked_place]);
        $query->andFilterWhere(['like', Organization::tableName() . '.title', $this->title]);
        $query->andFilterWhere(['like', VideoArchiveSearch::tableName() . '.drive_id', $this->drive_id.'%', false]);
        $query->andFilterWhere(['like', VideoArchiveSearch::tableName() . '.worked_organization', $this->worked_organization]);
        $query->andFilterWhere(['like', VideoArchiveSearch::tableName() . '.worked_vehicle_number', $this->worked_vehicle_number]);

        //$query->joinWith(['workedOrganization' => function ($q) {
        //    $q->where(Organization::tableName() . '.title LIKE "%' . $this->workedOrganization . '%"');
        //}]);

        return $dataProvider;
    }
}
